using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControlM : MonoBehaviour
{
    public IEnumerator coroutine;

    public LayerMask whatIsGround;
    public Vector3 walkPoint;
    public float walkPointRange;
    public bool walkPointSet;
    public bool walking;
    public bool idling;

    public float idleTime;
    public float timeBetweenIddles;
    public NavMeshAgent navMeshAgent;

    public bool hasIdled;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("start");
    }
    private void Update()
    {
        if (walking)
        {
            navMeshAgent.isStopped = false;

            if (!walkPointSet)
            {
                SearchWalkPoint();

            }

            if (walkPointSet)
            {
                // JustWalk();
                navMeshAgent.SetDestination(walkPoint);
                Vector3 distanceToWalkPoint = transform.position - walkPoint;
                if (distanceToWalkPoint.magnitude < 1f)
                    //Walkpoint reached
                    walkPointSet = false;

            }
        }
       
    }
    // Update is called once per frame

    IEnumerator GoIdle()
    {
        navMeshAgent.isStopped = true;
        Debug.Log("on idle enumerator");
        idling = true;
        walking = false;
        hasIdled = true;
        yield return new WaitForSeconds(idleTime);
        walking = true;
    }

    public IEnumerator IdleBetween()
    {
        hasIdled = false;

        yield return new WaitForSeconds(timeBetweenIddles);

    }
    private void JustWalk()
    {
        navMeshAgent.SetDestination(walkPoint);
        Vector3 distanceToWalkPoint = transform.position - walkPoint;
        if (distanceToWalkPoint.magnitude < 1f)
            //Walkpoint reached
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }
    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bench" && !hasIdled)
        {
            Debug.Log("ACTIVA IDLE");

            StartCoroutine(GoIdle());
        }

    }
}
