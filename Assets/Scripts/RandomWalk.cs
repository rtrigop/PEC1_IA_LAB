using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;




public class RandomWalk : MonoBehaviour
{

    [Tooltip("Move speed of the character in m/s")]
    public float MoveSpeed;

    //private bool chasing;
    public LayerMask whatIsGround;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    public Animator animator;



    void Start()
    {

        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        GenerateSpeed();
    }
    private void Update()
    {

    }

    private void GenerateSpeed()
    {
        MoveSpeed = Random.Range(0.5f, 4.0f);
    }

    private void Patroling()
    {
        animator.SetBool("Running", false);

        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            navMeshAgent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }


}


