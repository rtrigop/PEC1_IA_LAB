using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PeopleController : MonoBehaviour
{
    
    public Transform Enemy;
    public Animator animator;
    [Tooltip("Move speed of the character in m/s")]
    public float MoveSpeed = 2.0f;

    [Tooltip("Sprint speed of the character in m/s")]
    public float SprintSpeed = 4.335f;
    private bool patrolState;
    private bool chasing;
    public LayerMask whatIsGround, whatIsEnemy;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    //public Light myLight;

    public float life = 10;
    public float rotationTime = 3.0f;
    public Transform[] wayPoints;
    //public AudioSource audiosource;
    public bool dead;
    private int nextWayPoint;
    //States
    public float sightRange;
    public bool enemyInSightRange;

  
    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    void Start()
    {

        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

    }
    private void Update()
    {
        //Check for sight range
        enemyInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsEnemy);

        if (!enemyInSightRange) Patroling();
        if (enemyInSightRange) RunAway();


    }


    private void Patroling()
    {
        animator.SetBool("Running", false);

        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            navMeshAgent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void RunAway()
    {
        animator.SetBool("Running", true);

        //transform.LookAt(Player);
        navMeshAgent.SetDestination(Enemy.position * -1);

    }




    private void DestroyNPC()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }




















}
