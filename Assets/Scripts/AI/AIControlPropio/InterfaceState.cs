using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface InterfaceState 
{
    void UpdateState();
    void GoToWanderState();
    void GoToIdleState();

    void OnTriggerEnter(Collider col);
    void OnTriggerExit(Collider col);

}
