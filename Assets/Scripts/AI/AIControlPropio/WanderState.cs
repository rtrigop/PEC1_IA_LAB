using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class WanderState : InterfaceState
{
    AIControler wanderer;
    float currentWalkTime = 0;


    public WanderState(AIControler wander)
    {
        wanderer = wander;
    }

    // Update is called once per frame
    public void UpdateState()
    {

        if (currentWalkTime >= wanderer.timeBetweenIddles)
        {
            wanderer.hasIdled = false;

        }

        if (!wanderer.walkPointSet)
        {
            SearchWalkPoint();

        }

        if (wanderer.walkPointSet)
        {
            // JustWalk();
            wanderer.navMeshAgent.SetDestination(wanderer.walkPoint);
            Vector3 distanceToWalkPoint = wanderer.transform.position - wanderer.walkPoint;
            if (distanceToWalkPoint.magnitude < 1f)
                //Walkpoint reached
                wanderer.walkPointSet = false;

        }


        currentWalkTime += Time.deltaTime;

    }

    private void JustWalk()
    {
        wanderer.navMeshAgent.SetDestination(wanderer.walkPoint);
        Vector3 distanceToWalkPoint = wanderer.transform.position - wanderer.walkPoint;
        if (distanceToWalkPoint.magnitude < 1f)
            //Walkpoint reached
            wanderer.walkPointSet = false;

    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-wanderer.walkPointRange, wanderer.walkPointRange);
        float randomX = Random.Range(-wanderer.walkPointRange, wanderer.walkPointRange);

        wanderer.walkPoint = new Vector3(wanderer.transform.position.x + randomX, wanderer.transform.position.y, wanderer.transform.position.z + randomZ);

        if (Physics.Raycast(wanderer.walkPoint, -wanderer.transform.up, 2f, wanderer.whatIsGround))
            wanderer.walkPointSet = true;
    }

    public void GoToIdleState()
    {


        wanderer.navMeshAgent.isStopped = true;
        wanderer.animator.SetBool("idle", true);

        wanderer.currentState = wanderer.idleState;
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bench" && !wanderer.hasIdled)
        {
            GoToIdleState();
        }

    }


    public void OnTriggerExit(Collider col) { }


    public void GoToWanderState()
    {
    }





}
