using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class AIControler : MonoBehaviour
{
    [HideInInspector] public IdleState idleState;
    [HideInInspector] public WanderState wanderState;
    [HideInInspector] public InterfaceState currentState;

    [HideInInspector] public NavMeshAgent navMeshAgent;
    public Animator animator;

    public IEnumerator coroutine;

    public LayerMask whatIsGround;
    public Vector3 walkPoint;
    public float walkPointRange;
    public bool walkPointSet;


  //  public Transform[] wayPoints;


    public float idleTime = 0.5f;
    public float timeBetweenIddles = 7.0f;

    public bool hasIdled;

    void Start()
    {
        // Creamos los estados de nuestra IA.
        wanderState = new WanderState(this);
        idleState = new IdleState(this);

        // Le decimos que inicialmente empezará caminando
        currentState = wanderState; 
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = gameObject.GetComponent<Animator>();

    }

    void Update()
    {
        // Como nuestros estados no heredan de
        // MonoBehaviour, no se llama a su update 
        // automáticamente, y nos encargaremos 
        // nosotros de llamarlo a cada frame.
        currentState.UpdateState();
    }

    // Ya que nuestros states no heredan de 
    // MonoBehaviour, tendremos que avisarles
    // cuando algo entra, está o sale de nuestro
    // trigger.
    void OnTriggerEnter(Collider col)
    {
        currentState.OnTriggerEnter(col);
    }


    void OnTriggerExit(Collider col)
    {
        currentState.OnTriggerExit(col);
    }


}
