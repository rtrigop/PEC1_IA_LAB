using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class IdleState : InterfaceState
{
    AIControler wanderer;
    float currentIdleTime = 0;

    public IdleState(AIControler wander)
    {
        wanderer = wander;
    }

    public void UpdateState()
    {


        if (currentIdleTime >= wanderer.idleTime)
        {
            currentIdleTime = 0;

            GoToWanderState();

        }
        else
        {
            
            wanderer.hasIdled = true;

        }


        currentIdleTime += Time.deltaTime;

    }




    public void GoToIdleState()
    { }
    public void GoToWanderState()
    {
        wanderer.navMeshAgent.isStopped = false;
        wanderer.animator.SetBool("idle", false);
        wanderer.currentState = wanderer.wanderState;
    }

    public void OnTriggerEnter(Collider col)
    {
    }

    public void OnTriggerExit(Collider col)
    {
    }




}
